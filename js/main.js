let tabName;

let tabs = function () {
    const tabTitle = document.querySelectorAll('.tabs-title');
    const tabTxt = document.querySelectorAll('.tabs-content-text');
    // tabTitle.forEach(item => {
    //     item.addEventListener('click', selectTab);
    // });
    for (let item of tabTitle) {
        item.addEventListener('click', selectTab);
    }

    function selectTab() {
        tabTitle.forEach(elem => {
            elem.classList.remove('active');

        });
        this.classList.add('active');
        tabName = this.getAttribute('data-tab-name');
        for (let el of tabTxt) {
            if (el.classList.contains(tabName)) {
                el.classList.add('active-tab');
            } else el.classList.remove('active-tab');
        }
    }

};

tabs();